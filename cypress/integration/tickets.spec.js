describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))
    
    it("Preencher campos de texto", () => {
        const firstName = "Pedro";
        const lastName = "Gonzaga";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("pedro.test@gmail.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    })

    it("Select", () => {
        cy.get("#ticket-quantity").select("2")
    })

    it("Radio Button", () => {
        cy.get("#vip").check();
    })

    it("Checkbox", () => {
        cy.get("#social-media").check();
    })

    it("Checkbox Check e Uncheck", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    })

    it("Validar Titulo", () => {
        cy.get("header h1").should("contain", "TICKETBOX")
    })

    it("Validar email invalido", () => {
        cy.get("#email")
        .as("email")
        .type("test-gmail");

        cy.get("#email.invalid").should("exist");
      
        cy.get("@email")
        .clear()
        .type("teste@gmail.com");

        cy.get("#email.invalid").should("not.exist")
    })

    it("Preencher formulario e resetar", () => {
        const firstName = "Pedro";
        const lastName = "Gonzaga";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("pedro.test@gmail.com");
        cy.get("#ticket-quantity").select("2")
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(fullName);

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets`
        );

        cy.get("#agree").click();

        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");

        cy.get("button[type='reset']").click();
        cy.get("@submitButton").should("be.disabled")
    })

    it("Preenche campos obrigatorios", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@gmail.com"
        };

        cy.preencherCamposObrigatorios(customer);
        
    })
})